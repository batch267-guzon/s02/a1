<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>
	<h4>Divisible of Five</h4>
	<p><?php echo printDivisibleOfFive() ?></p>

	<h4>Array Manipulation</h4>
	<?php array_push($students, "John Smith"); ?>
	<p><?php echo var_dump($students) ?></p>
	<p><?php echo count($students) ?></p>

	<?php array_push($students, "Jane Smith"); ?>
	<p><?php echo var_dump($students) ?></p>
	<p><?php echo count($students) ?></p>

	<?php array_shift($students); ?>
	<p><?php echo var_dump($students) ?></p>
	<p><?php echo count($students) ?></p>
</body>
</body>
</html>